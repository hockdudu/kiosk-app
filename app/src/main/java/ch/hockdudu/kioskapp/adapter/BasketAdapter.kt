package ch.hockdudu.kioskapp.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import ch.hockdudu.kioskapp.R
import ch.hockdudu.kioskapp.helper.basket.BasketItem
import ch.hockdudu.kioskapp.helper.credentials.Credentials
import ch.hockdudu.kioskapp.helper.utils.formatProductPrice
import ch.hockdudu.kioskapp.helper.utils.getGlideUrl
import com.bumptech.glide.Glide
import com.bumptech.glide.RequestManager

class BasketAdapter(private val basketItems: List<BasketItem>, private val credentials: Credentials, context: Context) : RecyclerView.Adapter<BasketAdapter.ViewHolder>() {
    private var glide: RequestManager = Glide.with(context)

    class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        private val name: TextView = view.findViewById(R.id.productName)
        private val quantity: TextView = view.findViewById(R.id.productQuantity)
        private val price: TextView = view.findViewById(R.id.productPrice)
        private val picture: ImageView = view.findViewById(R.id.productImage)

        private var basketItem: BasketItem? = null

        fun bind(basketItem: BasketItem, loadImage: (ImageView) -> Unit) {
            this.basketItem = basketItem

            this.name.text = basketItem.product.name
            this.quantity.text = basketItem.quantity.toString()
            this.price.text = formatProductPrice(basketItem.product.price)

            loadImage(this.picture)
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(parent.context)
            .inflate(R.layout.basket_item, parent, false)

        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val basketItem = basketItems[position]

        holder.bind(basketItem) {
            glide.load(getGlideUrl(basketItem.product, credentials)).into(it)
        }
    }

    override fun getItemCount(): Int = basketItems.size
}