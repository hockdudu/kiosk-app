package ch.hockdudu.kioskapp.adapter

import android.annotation.SuppressLint
import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import ch.hockdudu.kioskapp.R
import ch.hockdudu.kioskapp.helper.credentials.Credentials
import ch.hockdudu.kioskapp.helper.store.Store
import ch.hockdudu.kioskapp.helper.utils.formatProductPrice
import ch.hockdudu.kioskapp.helper.utils.getGlideUrl
import ch.hockdudu.kioskapp.model.Product
import com.bumptech.glide.Glide
import com.bumptech.glide.RequestManager

class ProductAdapter(
    private val store: Store,
    private val credentials: Credentials,
    private val onClick: (Product) -> Unit,
    context: Context
) : RecyclerView.Adapter<ProductAdapter.ViewHolder>() {

    private var glide: RequestManager = Glide.with(context)

    class ViewHolder(view: View, val onClick: (Product) -> Unit) : RecyclerView.ViewHolder(view) {
        private val name: TextView = view.findViewById(R.id.productName)
        private val category: TextView = view.findViewById(R.id.productCategory)
        private val price: TextView = view.findViewById(R.id.productPrice)
        private val picture: ImageView = view.findViewById(R.id.productImage)
        
        private var product: Product? = null
        
        init {
            view.setOnClickListener { 
                product?.let {
                    onClick(it)
                }
            }
        }
        
        @SuppressLint("SetTextI18n")
        fun bind(product: Product, loadImage: (ImageView) -> Unit) {
            this.name.text = product.name
            this.category.text = product.category.name
            this.price.text = formatProductPrice(product.price)
            loadImage(this.picture)
            
            this.product = product
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(parent.context)
            .inflate(R.layout.product_item, parent, false)

        return ViewHolder(view, onClick)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val product = store.getProducts()[position]
        
        holder.bind(product) {
            glide.load(getGlideUrl(product, credentials)).into(it)
        }
    }

    override fun getItemCount(): Int = store.getProducts().size
}