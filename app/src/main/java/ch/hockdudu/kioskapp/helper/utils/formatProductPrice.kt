package ch.hockdudu.kioskapp.helper.utils

import java.text.DecimalFormat

private val decimalFormat = DecimalFormat("0.00")

fun formatProductPrice(price: Int): String = decimalFormat.format(price / 100f)