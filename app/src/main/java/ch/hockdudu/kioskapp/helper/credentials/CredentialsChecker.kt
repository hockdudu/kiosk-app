package ch.hockdudu.kioskapp.helper.credentials

import android.os.Handler
import ch.hockdudu.kioskapp.helper.api.Endpoint
import ch.hockdudu.kioskapp.helper.api.UrlFetcher

class CredentialsChecker(private val urlFetcher: UrlFetcher) {
    interface Listener {
        fun onValidCredentials()
        fun onInvalidCredentials()
    }

    fun checkCredentials(credentials: Credentials, listener: Listener) {
        val handler = Handler()
        Thread {
            val result = this.urlFetcher.fetchUrl(Endpoint.user(credentials.userId))

            if (result != null) {
                handler.post { listener.onValidCredentials() }
            } else {
                handler.post { listener.onInvalidCredentials() }
            }
        }.start()
    }

}