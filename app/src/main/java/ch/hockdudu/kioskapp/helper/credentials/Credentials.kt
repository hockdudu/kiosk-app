package ch.hockdudu.kioskapp.helper.credentials

data class Credentials(var authorizationHeader: String, var userId: String) {
    fun update(credentials: Credentials) {
        this.authorizationHeader = credentials.authorizationHeader
        this.userId = credentials.userId
    }
}
