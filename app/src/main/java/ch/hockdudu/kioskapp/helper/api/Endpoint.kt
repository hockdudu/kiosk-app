package ch.hockdudu.kioskapp.helper.api

import ch.hockdudu.kioskapp.model.Category
import ch.hockdudu.kioskapp.model.Product

object Endpoint {
    const val BASE_DOMAIN = "https://shop.dotmob.dev"

    fun user(userId: String): String = "/api/kiosk/customers/$userId"
    fun categories(userId: String): String = "/api/kiosk/categories?customerId=${userId}"
    fun products(userId: String, category: Category) = "/api/kiosk/categories/${category.id}/products?customerId=${userId}"
    fun image(product: Product) = "/api/kiosk/products/${product.id}/picture"

    fun purchase(userId: String) = "/api/kiosk/orders?customerId=${userId}"
}