package ch.hockdudu.kioskapp.helper.credentials

import android.content.Context
import android.content.SharedPreferences
import androidx.core.content.edit

fun getCredentials(context: Context): Credentials? {
    val prefs = getPreferences(context)

    val authorizationHeader = prefs.getString("authorization_header", null) ?: return null
    val userId = prefs.getString("user_id", null) ?: return null

    return Credentials(authorizationHeader, userId)
}

fun setCredentials(context: Context, credentials: Credentials) {
    val prefs = getPreferences(context)
    prefs.edit {
        this.putString("authorization_header", credentials.authorizationHeader)
        this.putString("user_id", credentials.userId)
    }
}

fun eraseCredentials(context: Context) {
    val prefs = getPreferences(context)

    prefs.edit {
        this.remove("authorization_header")
        this.remove("user_id")
    }
}

private fun getPreferences(context: Context): SharedPreferences {
    return context.getSharedPreferences(getPreferencesName(context), Context.MODE_PRIVATE)
}

private fun getPreferencesName(context: Context): String {
    return context.packageName + "_credentials";
}