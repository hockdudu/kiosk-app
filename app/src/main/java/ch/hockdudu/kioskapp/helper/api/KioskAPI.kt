package ch.hockdudu.kioskapp.helper.api

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import ch.hockdudu.kioskapp.helper.credentials.Credentials
import ch.hockdudu.kioskapp.model.Category
import ch.hockdudu.kioskapp.model.Product
import ch.hockdudu.kioskapp.model.User

class KioskAPI(private val credentials: Credentials) {
    private val urlFetcher: UrlFetcher = UrlFetcher(credentials)
    private val parser: Parser = Parser()

    fun getUserInfo(userId: String): LiveData<User> {
        val liveData = MutableLiveData<User>()

        Thread {
            val response = this.urlFetcher.fetchUrl(Endpoint.user(userId))

            if (response != null) {
                val user = this.parser.parseUserInfo(response)

                if (user != null) {
                    liveData.postValue(user)
                }
            }
        }.start()

        return liveData
    }

    fun getCategories(userId: String): LiveData<List<Category>> {
        val liveData = MutableLiveData<List<Category>>()

        Thread {
            val response = this.urlFetcher.fetchUrl(Endpoint.categories(userId))

            if (response != null) {
                val categories = this.parser.parseCategories(response)

                if (categories != null) {
                    liveData.postValue(categories)
                }
            }

        }.start()

        return liveData
    }

    fun getProducts(userId: String, category: Category): LiveData<List<Product>> {
        val liveData = MutableLiveData<List<Product>>()

        Thread {
            val response = this.urlFetcher.fetchUrl(Endpoint.products(userId, category))

            if (response != null) {
                val products = this.parser.parseProducts(response, category)

                if (products != null) {
                    liveData.postValue(products)
                }
            }
        }.start()

        return liveData
    }

    fun executePurchase(userId: String, products: Map<Int, Int>): LiveData<Boolean> {
        val liveData = MutableLiveData<Boolean>()

        Thread {
            val dataString = this.parser.buildPurchaseString(products)
            val response = this.urlFetcher.postUrl(Endpoint.purchase(userId), dataString)

            liveData.postValue(response != null)
        }.start()

        return liveData
    }
}