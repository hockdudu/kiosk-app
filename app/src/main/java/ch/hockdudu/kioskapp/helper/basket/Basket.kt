package ch.hockdudu.kioskapp.helper.basket

import ch.hockdudu.kioskapp.model.Product

class Basket {
    private val basket: MutableMap<Int, BasketItem> = mutableMapOf()

    val total: Int
    get() {
        return getList().fold(0) { acc, basketItem ->
            acc + basketItem.product.price * basketItem.quantity
        }
    }

    fun add(product: Product) {
        if (basket.contains(product.id)) {
            basket[product.id]!!.quantity += 1
        } else {
            basket[product.id] = BasketItem(product, 1)
        }
    }

    fun get(): Map<Int, BasketItem> {
        return basket.toMap()
    }

    fun getIntMap(): Map<Int, Int> {
        return basket.mapValuesTo(mutableMapOf(), {
            it.value.quantity
        })
    }

    fun getList(): List<BasketItem> {
        return basket.values.toList()
    }

    fun remove(product: Product) {
        if (basket.contains(product.id)) {
            basket[product.id]!!.quantity -= 1
        }
    }

    fun clear() {
        basket.clear()
    }
}