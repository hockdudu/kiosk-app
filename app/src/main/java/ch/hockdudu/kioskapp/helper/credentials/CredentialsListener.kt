package ch.hockdudu.kioskapp.helper.credentials

fun interface CredentialsListener {
    fun receivedCredentials(credentials: Credentials)
}