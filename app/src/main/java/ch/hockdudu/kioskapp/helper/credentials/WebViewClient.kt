package ch.hockdudu.kioskapp.helper.credentials

import android.webkit.WebResourceRequest
import android.webkit.WebResourceResponse
import android.webkit.WebView
import android.webkit.WebViewClient

class WebViewClient(private val credentialsListener: CredentialsListener) : WebViewClient() {

    private val customerPageRegex = Regex("/api/kiosk/customers/([0-9a-fA-F-]+)")

    override fun shouldInterceptRequest(
        view: WebView?,
        request: WebResourceRequest?
    ): WebResourceResponse? {
        request?.apply {
            val credentials = getRequestCredentials(request) ?: return null
            this@WebViewClient.credentialsListener.receivedCredentials(credentials)
        }

        return super.shouldInterceptRequest(view, request)
    }

    private fun getRequestCredentials(request: WebResourceRequest): Credentials? {
        if (request.method != "GET") return null
        val authorization = request.requestHeaders["authorization"] ?: return null

        val path = request.url.path ?: return null
        val matches = customerPageRegex.matchEntire(path) ?: return null

        val userId = matches.groupValues[1]

        return Credentials(authorization, userId)
    }
}
