package ch.hockdudu.kioskapp.helper.basket

import ch.hockdudu.kioskapp.model.Product

data class BasketItem(val product: Product) {

    constructor(product: Product, quantity: Int) : this(product) {
        this.quantity = quantity
    }

    var quantity: Int = 0
        set(value) {
            if (value >= 0) field = value
        }
}