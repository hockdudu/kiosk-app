package ch.hockdudu.kioskapp.helper.store

import ch.hockdudu.kioskapp.model.Category
import ch.hockdudu.kioskapp.model.Product

class Store {
    private val categories: MutableSet<Category> = mutableSetOf()
    private val products: MutableMap<Category, MutableSet<Product>> = mutableMapOf()

    fun addCategory(category: Category) {
        this.categories.add(category)
    }

    fun addCategories(categories: Collection<Category>) {
        for (category in categories) {
            addCategory(category)
        }
    }

    fun addProduct(product: Product, category: Category) {
        if (category !in this.products) {
            this.products[category] = mutableSetOf()
        }

        this.products[category]!! += product
    }

    fun addProducts(products: Collection<Product>, category: Category) {
        for (product in products) {
            addProduct(product, category)
        }
    }

    fun getCategories(): List<Category> {
        return categories.toList()
    }

    fun getProducts(): List<Product> {
        return products.flatMap { (_, value) ->
            value.toList()
        }.sortedBy { product -> this.categories.indexOf(product.category) }
    }

    fun clearProducts() {
        this.products.clear()
    }

    fun clearCategories() {
        this.categories.clear()
    }
}