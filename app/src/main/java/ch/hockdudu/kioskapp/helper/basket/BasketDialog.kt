package ch.hockdudu.kioskapp.helper.basket

import android.app.AlertDialog
import android.app.Dialog
import android.content.DialogInterface
import android.view.LayoutInflater
import android.widget.TextView
import androidx.activity.ComponentActivity
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import ch.hockdudu.kioskapp.R
import ch.hockdudu.kioskapp.adapter.BasketAdapter
import ch.hockdudu.kioskapp.helper.credentials.Credentials
import ch.hockdudu.kioskapp.helper.utils.formatProductPrice

class BasketDialog(
    private val activity: ComponentActivity,
    private val credentials: Credentials,
    private val basket: Basket,
) {
    var positiveButtonAction = DialogInterface.OnClickListener { _, _ ->  }
    var negativeButtonAction = DialogInterface.OnClickListener { _, _ ->  }
    var neutralButtonAction = DialogInterface.OnClickListener { _, _ ->  }

    private fun createDialog(): Dialog {
        val builder = AlertDialog.Builder(activity)

        val adapterView = LayoutInflater.from(builder.context).inflate(R.layout.basket_adapter, null, false)

        val basketRecyclerView = adapterView.findViewById<RecyclerView>(R.id.recyclerView)
        basketRecyclerView.layoutManager = LinearLayoutManager(activity)
        basketRecyclerView.adapter = BasketAdapter(basket.getList(), credentials, activity)

        val totalPriceView = adapterView.findViewById<TextView>(R.id.totalPrice)
        totalPriceView.text = formatProductPrice(basket.total)

        builder.apply {
            setTitle(R.string.basket_confirm)
            setView(adapterView)
            setPositiveButton(R.string.basket_buy, positiveButtonAction)
            setNegativeButton(R.string.cancel, negativeButtonAction)
            setNeutralButton(R.string.basket_empty, neutralButtonAction)
        }

        return builder.create()
    }

    fun showDialog() {
        createDialog().show()
    }
}