package ch.hockdudu.kioskapp.helper.api

import ch.hockdudu.kioskapp.helper.credentials.Credentials
import java.io.IOException
import java.lang.Exception
import java.net.HttpURLConnection
import java.net.URL

class UrlFetcher(private val credentials: Credentials){
    fun fetchUrl(path: String): String? {
        val url = URL(Endpoint.BASE_DOMAIN + path)
        val connection = url.openConnection() as HttpURLConnection

        connection.setRequestProperty("authorization", this.credentials.authorizationHeader)

        val responseCode: Int
        try {
            responseCode = connection.responseCode
        } catch (e: IOException) {
            return null
        }

        return when(responseCode) {
            HttpURLConnection.HTTP_OK -> {
                return String(connection.inputStream.readBytes())
            }
            else -> {
                null
            }
        }
    }

    fun postUrl(path: String, data: String): String? {
        val url = URL(Endpoint.BASE_DOMAIN + path)
        val connection = url.openConnection() as HttpURLConnection

        connection.setRequestProperty("authorization", this.credentials.authorizationHeader)
        connection.setRequestProperty("content-type", "application/json; charset=utf-8")
        connection.requestMethod = "POST"
        connection.doOutput = true

        connection.outputStream.write(data.toByteArray())

        val responseCode: Int
        try {
            responseCode = connection.responseCode
        } catch (e: IOException) {
            return null
        }

        return when(responseCode) {
            HttpURLConnection.HTTP_OK,
            HttpURLConnection.HTTP_CREATED -> {
                return String(connection.inputStream.readBytes())
            }
            else -> {
                null
            }
        }
    }
}