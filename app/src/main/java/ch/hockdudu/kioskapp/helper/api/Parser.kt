package ch.hockdudu.kioskapp.helper.api

import ch.hockdudu.kioskapp.model.Category
import ch.hockdudu.kioskapp.model.Product
import ch.hockdudu.kioskapp.model.User
import org.json.JSONArray
import org.json.JSONException
import org.json.JSONObject

class Parser {
    fun parseUserInfo(userInfo: String): User? {
        return try {
            val reader = JSONObject(userInfo)

            User(
                reader.getString("Id"),
                reader.getString("DisplayName"),
                reader.getString("Initials"),
                reader.getString("GivenName"),
                reader.getString("Surname"),
            )
        } catch (e: JSONException) {
            null
        }
    }

    fun parseCategories(categories: String): List<Category>? {
        return try {
            val reader = JSONArray(categories)
            val categoryList = mutableListOf<Category>()
            for (i in 0 until reader.length()) {
                val category = parseCategory(reader.getJSONObject(i)) ?: continue
                categoryList += category
            }

            categoryList
        } catch (e: JSONException) {
            null
        }
    }

    fun parseProducts(products: String, category: Category): List<Product>? {
        return try {
            val reader = JSONArray(products)
            val productList = mutableListOf<Product>()
            for (i in 0 until reader.length()) {
                val product = parseProduct(reader.getJSONObject(i), category) ?: continue
                productList += product
            }

            productList
        } catch (e: JSONException) {
            null
        }
    }

    fun buildPurchaseString(products: Map<Int, Int>): String {
        val data = JSONObject()

        val lines = JSONArray()
        products.forEach {
            val product = JSONObject()
            product.put("productId", it.key)
            product.put("quantity", it.value)

            lines.put(product)
        }

        data.put("lines", lines)

        return data.toString()
    }

    private fun parseCategory(categoryReader: JSONObject): Category? {
        return try {
            Category(
                categoryReader.getInt("Id"),
                categoryReader.getString("Name")
            )
        } catch (e: JSONException) {
            return null
        }
    }

    private fun parseProduct(productReader: JSONObject, category: Category): Product? {
        return try {
            Product(
                productReader.getInt("Id"),
                productReader.getString("Name"),
                (productReader.getDouble("Price") * 100).toInt(),
                category
            )
        } catch (e: JSONException) {
            return null
        }
    }
}