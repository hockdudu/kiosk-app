package ch.hockdudu.kioskapp.helper.utils

import ch.hockdudu.kioskapp.helper.api.Endpoint
import ch.hockdudu.kioskapp.helper.credentials.Credentials
import ch.hockdudu.kioskapp.model.Product
import com.bumptech.glide.load.model.GlideUrl

fun getGlideUrl(product: Product, credentials: Credentials): GlideUrl {
    return GlideUrl(Endpoint.BASE_DOMAIN + Endpoint.image(product)) {
        mapOf(Pair("authorization", credentials.authorizationHeader))
    }
}