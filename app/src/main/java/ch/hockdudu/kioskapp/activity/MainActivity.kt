package ch.hockdudu.kioskapp.activity

import android.app.Activity
import android.content.DialogInterface
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import android.webkit.CookieManager
import androidx.annotation.StringRes
import androidx.recyclerview.widget.DefaultItemAnimator
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.RecyclerView
import ch.hockdudu.kioskapp.R
import ch.hockdudu.kioskapp.adapter.ProductAdapter
import ch.hockdudu.kioskapp.helper.api.KioskAPI
import ch.hockdudu.kioskapp.helper.api.UrlFetcher
import ch.hockdudu.kioskapp.helper.basket.Basket
import ch.hockdudu.kioskapp.helper.basket.BasketDialog
import ch.hockdudu.kioskapp.helper.credentials.Credentials
import ch.hockdudu.kioskapp.helper.credentials.CredentialsChecker
import ch.hockdudu.kioskapp.helper.credentials.eraseCredentials
import ch.hockdudu.kioskapp.helper.credentials.getCredentials
import ch.hockdudu.kioskapp.helper.store.Store
import ch.hockdudu.kioskapp.model.Product
import com.google.android.material.floatingactionbutton.FloatingActionButton
import com.google.android.material.snackbar.BaseTransientBottomBar
import com.google.android.material.snackbar.Snackbar

class MainActivity : AppCompatActivity() {
    companion object {
        private const val ACTIVITY_RESULT_LOGIN = 0;
    }

    private val credentials = Credentials("", "")

    private lateinit var api: KioskAPI

    private val store = Store();

    private val basket: Basket = Basket()

    private lateinit var productsRecyclerView: RecyclerView
    private lateinit var floatingBasketButton: FloatingActionButton

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        this.checkCredentials()

        productsRecyclerView = findViewById(R.id.productsRecyclerView)
        productsRecyclerView.layoutManager = GridLayoutManager(this, 2)
        productsRecyclerView.adapter = ProductAdapter(store, credentials, { onClickProduct(it) }, this)
        productsRecyclerView.itemAnimator = DefaultItemAnimator()

        floatingBasketButton = findViewById(R.id.basketFAB)
        floatingBasketButton.setOnClickListener {
            openBasket()
        }
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.main_menu, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return when (item.itemId) {
            R.id.logout -> {
                executeLogout()
                true
            }
            else -> super.onOptionsItemSelected(item);
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        when(requestCode) {
            ACTIVITY_RESULT_LOGIN -> {
                if (resultCode == Activity.RESULT_OK) {
                    checkCredentials()
                }
            }
            else -> {
                super.onActivityResult(requestCode, resultCode, data)
            }
        }
    }

    private fun onClickProduct(product: Product) {
        basket.add(product)

        fun showSnackbar() {
            val snackbar = makeSnackbar(R.string.basket_added_product, Snackbar.LENGTH_SHORT)
            snackbar.setAction(R.string.close) { snackbar.dismiss() }
            snackbar.show()
        }

        if (floatingBasketButton.isShown) {
            showSnackbar()
        } else {
            floatingBasketButton.show(object : FloatingActionButton.OnVisibilityChangedListener() {
                override fun onShown(fab: FloatingActionButton?) = showSnackbar()
            })
        }
    }

    private fun openBasket() {
        val basketDialog = BasketDialog(this, credentials, basket)
        basketDialog.apply {
            positiveButtonAction = DialogInterface.OnClickListener { _, _ ->
                api.executePurchase(credentials.userId, basket.getIntMap()).observe(this@MainActivity, { successful ->
                    if (successful) {
                        makeSnackbar(R.string.basket_buy_successful, Snackbar.LENGTH_SHORT).show()
                        emptyBasket()
                    } else {
                        makeSnackbar(R.string.basket_buy_error, Snackbar.LENGTH_LONG).show()
                    }
                })
            }
            neutralButtonAction = DialogInterface.OnClickListener { _, _ ->
                emptyBasket()
            }
            showDialog()
        }
    }

    private fun checkCredentials() {
        val credentials = getCredentials(this)

        if (credentials == null) {
            this.updateCredentials()
        } else {
            CredentialsChecker(UrlFetcher(credentials))
                .checkCredentials(credentials, object : CredentialsChecker.Listener {
                    override fun onValidCredentials() {
                        this@MainActivity.apply {
                            this.api = KioskAPI(credentials)
                            this.credentials.update(credentials)
                            this.updateData()
                        }
                    }

                    override fun onInvalidCredentials() {
                        updateCredentials()
                    }
                })
        }
    }

    private fun updateCredentials() {
        val intent = Intent(this, LoginActivity::class.java)
        startActivityForResult(intent, ACTIVITY_RESULT_LOGIN)
    }

    private fun updateData() {
        this.api.getCategories(this.credentials.userId).observe(this, { categories ->
            this.store.clearCategories()
            this.store.clearProducts()

            this.productsRecyclerView.adapter?.notifyDataSetChanged()

            this.store.addCategories(categories)

            categories.forEach { category ->
                this.api.getProducts(credentials.userId, category).observe(this, { products ->
                    this.store.addProducts(products, category)
                    this.productsRecyclerView.adapter?.notifyDataSetChanged()
                })
            }
        })
    }

    private fun makeSnackbar(@StringRes resId: Int, @BaseTransientBottomBar.Duration duration: Int): Snackbar {
        return Snackbar.make(findViewById(R.id.coordinatorLayout), resId, duration)
            .setAnchorView(floatingBasketButton);
    }

    private fun emptyBasket() {
        basket.clear()
        floatingBasketButton.hide()
    }

    private fun executeLogout() {
        eraseCredentials(this)
        CookieManager.getInstance().removeAllCookies {
            this.updateCredentials()
        }
    }
}