package ch.hockdudu.kioskapp.activity

import android.annotation.SuppressLint
import android.app.Activity
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.webkit.CookieManager
import android.webkit.WebView
import ch.hockdudu.kioskapp.helper.credentials.Credentials
import ch.hockdudu.kioskapp.helper.credentials.CredentialsListener
import ch.hockdudu.kioskapp.helper.credentials.WebViewClient
import ch.hockdudu.kioskapp.helper.credentials.setCredentials

class LoginActivity : AppCompatActivity(), CredentialsListener {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        initializeAuthorizationProcess()
    }

    @SuppressLint("SetJavaScriptEnabled")
    private fun initializeAuthorizationProcess() {
        val loginWebView = WebView(this)
        setContentView(loginWebView)

        CookieManager.getInstance().setAcceptThirdPartyCookies(loginWebView, true)

        loginWebView.webViewClient = WebViewClient(this)
        loginWebView.settings.domStorageEnabled = true

        loginWebView.settings.javaScriptEnabled = true
        loginWebView.loadUrl("https://pos.dotmob.dev")
    }

    override fun receivedCredentials(credentials: Credentials) {
        setCredentials(this, credentials)
        setResult(Activity.RESULT_OK)
        finish()
    }
}