package ch.hockdudu.kioskapp.model

data class Product(
    val id: Int,
    val name: String,
    val price: Int,
    val category: Category,
)
