package ch.hockdudu.kioskapp.model

data class User(
    val id: String,
    val displayName: String,
    val initials: String,
    val givenName: String,
    val surname: String
)
