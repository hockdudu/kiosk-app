# Kiosk API Documentation

## Authentication
1. Opens shop
2. Request to login.microsoftonline.com
2.1 https://login.microsoftonline.com/67a785af-9e6d-422c-bff5-ac13f6964625/v2.0/.well-known/openid-configuration
3. Call to `authorization_endpoint` from previous .well-known
3.1 client_id, scope, redirect_uri, and so on
4. Redirected from `authorization_endpoint` to `redirect_uri`
4.1 With Fragment #code=0.AAAA[...]&client_info=[...]&state=[...]session_state=[...]
5. Call to /appsettings.json and appsettings.Production.json (is it important?)
5.1 Gets info like `BaseUri: "https:/shop.dotmob.dev/"`
5.2 The login.microsoftonline.com ID is also there
6. Second superfluous call to login.microsoftonline.com
7. Call to `token_endpoint`
7.1 With `client_id`, `scope` of `appsettings.json`, `code` of `authorization_endpoint`, and other stuff..?
8. Gets response with `acess_token`, `refresh_token` (after token has expired?), `expires_in` (in seconds, here always one hour) and other stuff
8.1 `access_token` ist from now on our `Bearer` authorization
9. Call to `/api/kiosk/customers/[ID]`
9.1 `[ID]` is decoded from `access_token` JWT

### Token refresh
`[TBD]`

## Products
### Categories
- Needs authorization
- Customer ID is optional
```
/api/kiosk/categories?customerId=[ID]
```
```json
[
  {
    "Id": 2,
    "Name": "Backwaren"
  },
  {
    "Id": 1,
    "Name": "Red Bull"
  },
  {
    "Id": 6,
    "Name": "Spenden"
  }
]
```

### Products
- Needs authorization
- Customer ID is required
```
/api/kiosk/categories/[ID]/products?customerId=[ID]
```
```json
[
  {
    "Id": 40,
    "Name": "Berger Linzertörtli",
    "Price": 1.0000
  },
  {
    "Id": 42,
    "Name": "Berger Spitzbueb",
    "Price": 1.0000
  },
  {
    "Id": 41,
    "Name": "Berger Vogelnestli",
    "Price": 1.0000
  }
]
```

### Picture
- Needs authorization
```
/api/kiosk/products/40/picture
```
```
[binary image response]
```

### Payment
- Needs authorization
- Maybe needs customer ID (more tests needed)
- `content-type: application/json`
```
/api/kiosk/orders?customerId=[ID]
```
```json
{
  "lines": [
    {
      "productId": 41,
      "quantity": 2
    },
    {
      "productId": 42,
      "quantity": 1
    }
  ]
}
```

### Customers
- Needs authorization
```
/api/kiosk/customers/[ID]
```
```json
{
  "Id": "4786bea0-41ec-4f9f-9a30-ee571263a8aa",
  "DisplayName": "Eduardo Rocha",
  "Initials": "ER",
  "GivenName": "Eduardo",
  "Surname": "Rocha"
}
```